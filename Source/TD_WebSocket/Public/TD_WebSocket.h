#pragma once

#include "Modules/ModuleManager.h"

class FTD_WebSocketModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
