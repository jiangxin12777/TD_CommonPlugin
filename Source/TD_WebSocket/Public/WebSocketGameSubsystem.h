#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "IWebSocket.h"
#include "WebSocketGameSubsystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FWSGS_OnConnected);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWSGS_OnConnectionError, const FString&, Error);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FWSGS_OnClosed, int32, StatusCode, const FString& , Reason, bool, bWasClean);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWSGS_OnMessage, const FString&, Message);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWSGS_OnMessageSent, const FString&, MessageString);

/**
 * 
 */
UCLASS()
class UWebSocketGameSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	UWebSocketGameSubsystem();

	UFUNCTION(BlueprintCallable, Category = "Web Socket")
	void CreateWebSocket(const FString& NewUrl, const FString& NewProtocol = FString(TEXT("ws")));

	UFUNCTION(BlueprintCallable, Category = "Web Socket")
	void Connect();

	UFUNCTION(BlueprintCallable, Category = "Web Socket")
	void DisConnect();

	UFUNCTION(BlueprintCallable, Category = "Web Socket")
	void SendMessage(const FString& InStr);

	UFUNCTION(BlueprintCallable, Category = "Web Socket")
	bool IsConnected() const;

public:
	UPROPERTY(BlueprintAssignable)
	FWSGS_OnConnected EventOnConnected;

	UPROPERTY(BlueprintAssignable)
	FWSGS_OnConnectionError EventOnConnectionError;

	UPROPERTY(BlueprintAssignable)
	FWSGS_OnClosed EventOnClosed;

	UPROPERTY(BlueprintAssignable)
	FWSGS_OnMessage EventOnMessage;

	UPROPERTY(BlueprintAssignable)
	FWSGS_OnMessageSent EventOnMessageSent;

protected:
	/** Implement this for initialization of instances of the system */
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual bool ShouldCreateSubsystem(UObject* Outer) const override;

	/** Implement this for deinitialization of instances of the system */
	virtual void Deinitialize() override;

	void OnConnected();
	void OnConnectionError(const FString& Error);
	void OnClosed(int32 StatusCode,const FString& Reason,bool bWasClean);
	void OnMessage(const FString& Message); // 接收消息时
	void OnMessageSent(const FString& MessageString); // 发送消息时

	void ClearDelegate();

private:
	FString ServerURL;
	FString ServerProtocol = "ws";

	TSharedPtr<IWebSocket> Socket = nullptr;
};
