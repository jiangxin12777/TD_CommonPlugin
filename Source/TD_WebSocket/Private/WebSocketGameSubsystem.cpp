#include "WebSocketGameSubsystem.h"

#include "WebSocketsModule.h"

UWebSocketGameSubsystem::UWebSocketGameSubsystem()
{
}

void UWebSocketGameSubsystem::CreateWebSocket(const FString& NewUrl, const FString& NewProtocol)
{
	ServerURL = NewUrl;
	ServerProtocol = NewProtocol;

	UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(TEXT("UWebSocketGameSubsystem:: WebSocket CreateWebSocket")));
	UE_LOG(LogTemp, Warning, TEXT("%s"), *NewUrl);
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("UWebSocketGameSubsystem:: WebSocket %s"), *NewUrl));

	DisConnect();

	FWebSocketsModule WebSockets = FModuleManager::LoadModuleChecked<FWebSocketsModule>("WebSockets");

	UE_LOG(LogTemp, Warning, TEXT("%s--%p"), *FString(TEXT("UWebSocketGameSubsystem:: WebSocket CreateWebSocket")), &WebSockets);
	//UE_LOG(LogTemp, Warning, TEXT("%s--%p"), *FString(TEXT("UWebSocketGameSubsystem:: WebSocket CreateWebSocket")), &FWebSocketsModule::Get());
	
	Socket = WebSockets.CreateWebSocket(NewUrl, NewProtocol);
	Socket->OnConnected().AddUObject(this, &UWebSocketGameSubsystem::OnConnected);
	Socket->OnConnectionError().AddUObject(this, &UWebSocketGameSubsystem::OnConnectionError);
	Socket->OnClosed().AddUObject(this, &UWebSocketGameSubsystem::OnClosed);
	Socket->OnMessage().AddUObject(this, &UWebSocketGameSubsystem::OnMessage);
	Socket->OnMessageSent().AddUObject(this, &UWebSocketGameSubsystem::OnMessageSent);
}

void UWebSocketGameSubsystem::Connect()
{
	if (Socket != nullptr && !Socket->IsConnected())
	{
		Socket->Connect();
		UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(TEXT("UWebSocketGameSubsystem:: WebSocket Connect")));
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("WebSocket Connect")));
	}
}

void UWebSocketGameSubsystem::DisConnect()
{
	if (Socket != nullptr && Socket->IsConnected())
	{
		Socket->Close();
		Socket = nullptr;
		ClearDelegate();
		UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(TEXT("UWebSocketGameSubsystem:: WebSocket DisConnect")));
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("WebSocket DisConnect")));
	}
}

void UWebSocketGameSubsystem::SendMessage(const FString& InStr)
{
	if (Socket != nullptr && Socket->IsConnected())
	{
		Socket->Send(InStr);
	}
}

bool UWebSocketGameSubsystem::IsConnected() const
{
	if (Socket != nullptr)
	{
		return Socket->IsConnected();
	}
	return false;
}

void UWebSocketGameSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(TEXT("UWebSocketGameSubsystem:: WebSocket Initialize")));
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("WebSocket Initialize")));
}

bool UWebSocketGameSubsystem::ShouldCreateSubsystem(UObject* Outer) const
{
	const UGameInstance* GameInstance = CastChecked<UGameInstance>(Outer);
	const bool bIsServerWorld = GameInstance->IsDedicatedServerInstance();
	return !bIsServerWorld;
}

void UWebSocketGameSubsystem::Deinitialize()
{
	Super::Deinitialize();
	UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(TEXT("UWebSocketGameSubsystem:: WebSocket Deinitialize")));
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString::Printf(TEXT("WebSocket Deinitialize")));

	DisConnect();
}

void UWebSocketGameSubsystem::OnConnected()
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(__FUNCTION__));

	EventOnConnected.Broadcast();
}

void UWebSocketGameSubsystem::OnConnectionError(const FString& Error)
{
	UE_LOG(LogTemp, Warning, TEXT("%s Error:%s"), *FString(__FUNCTION__), *Error);

	EventOnConnectionError.Broadcast(Error);
}

void UWebSocketGameSubsystem::OnClosed(int32 StatusCode, const FString& Reason, bool bWasClean)
{
	UE_LOG(LogTemp,Warning,TEXT("%s StatusCode:%d Reason:%s bWasClean:%d"), *FString(__FUNCTION__),StatusCode,*Reason,bWasClean);

	EventOnClosed.Broadcast(StatusCode, Reason, bWasClean);
}

void UWebSocketGameSubsystem::OnMessage(const FString& Message)
{
	UE_LOG(LogTemp,Warning,TEXT("%s Message:%s"),*FString(__FUNCTION__),*Message);

	EventOnMessage.Broadcast(Message);
}

void UWebSocketGameSubsystem::OnMessageSent(const FString& MessageString)
{
	UE_LOG(LogTemp,Warning,TEXT("%s MessageString:%s"),*FString(__FUNCTION__),*MessageString);

	EventOnMessageSent.Broadcast(MessageString);
}

void UWebSocketGameSubsystem::ClearDelegate()
{
	EventOnConnected.Clear();
	EventOnConnectionError.Clear();
	EventOnClosed.Clear();
	EventOnMessage.Clear();
	EventOnMessageSent.Clear();
}
