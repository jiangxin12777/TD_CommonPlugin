#include "TD_WebSocket.h"

#define LOCTEXT_NAMESPACE "FTD_WebSocketModule"

void FTD_WebSocketModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	// Register settings
}

void FTD_WebSocketModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FTD_WebSocketModule, TD_WebSocket)