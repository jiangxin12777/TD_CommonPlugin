#pragma once

#include "CoreMinimal.h"
#include "TD_LoadingScreenManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPreLoadMapTriggered, const FString&, MapName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSeamlessTravelStart, const FString&, MapName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPostLoadMapTriggered);

UCLASS()
class TD_LOADINGSCREEN_API UTD_LoadingScreenManager : public UGameInstanceSubsystem, public FTickableGameObject
{
	GENERATED_BODY()

public:
	UTD_LoadingScreenManager();

	// ~USubsystem interface
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	virtual bool ShouldCreateSubsystem(UObject* Outer) const override;
	// ~End of USubsystem interface

	// ~FTickableObjectBase interface
	virtual void Tick(float DeltaTime) override;
	virtual ETickableTickType GetTickableTickType() const override;
	virtual bool IsTickable() const override;
	virtual TStatId GetStatId() const override;
	virtual UWorld* GetTickableGameObjectWorld() const override;
	// ~End of FTickableObjectBase interface

public:
	UFUNCTION(BlueprintCallable, Category = "TD|Loading Screen")
	void AddLayerToScreen(const int32 InLayerID, TSubclassOf<UUserWidget> InWidget, const int32 ZOrder = 1000);

	UFUNCTION(BlueprintCallable, Category = "TD|Loading Screen")
	void RemoveLayerToScreen(const int32 InLayerID);

public:
	UPROPERTY(BlueprintAssignable, Category = "TD|Loading Screen")
	FOnPreLoadMapTriggered OnPreLoadMapTriggered;
	UPROPERTY(BlueprintAssignable, Category = "TD|Loading Screen")
	FOnPostLoadMapTriggered OnPostLoadMapTriggered;
	UPROPERTY(BlueprintAssignable, Category = "TD|Loading Screen")
	FOnSeamlessTravelStart OnSeamlessTravelStart;

protected:
	void HandlePreLoadMapNoContext(const FString& MapName);
	void HandlePreLoadMap(const FWorldContext& WorldContext, const FString& MapName);
	void HandlePostLoadMap(UWorld* World);
	void HandleSeamlessTravelStart(UWorld* World, const FString& Map);

private:
	TMap<int32, TSharedPtr<SWidget>> WidgetLayersMap;
};