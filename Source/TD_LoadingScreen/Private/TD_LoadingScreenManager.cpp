#include "TD_LoadingScreenManager.h"

#include "Blueprint/UserWidget.h"

UTD_LoadingScreenManager::UTD_LoadingScreenManager()
{
}

void UTD_LoadingScreenManager::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

// #if UE_VERSION_NEWER_THAN(4,27,2)
// 	FCoreUObjectDelegates::PreLoadMapWithContext.AddUObject(this, &ThisClass::HandlePreLoadMap);
// #else
// 	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &ThisClass::HandlePreLoadMapNoContext);
// #endif
// 	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &ThisClass::HandlePostLoadMap);
//
// 	FWorldDelegates::OnSeamlessTravelStart.AddUObject(this, &ThisClass::HandleSeamlessTravelStart);
}

void UTD_LoadingScreenManager::Deinitialize()
{
	Super::Deinitialize();

	FCoreUObjectDelegates::PreLoadMap.RemoveAll(this);
	FCoreUObjectDelegates::PostLoadMapWithWorld.RemoveAll(this);
}

bool UTD_LoadingScreenManager::ShouldCreateSubsystem(UObject* Outer) const
{
	const UGameInstance* GameInstance = CastChecked<UGameInstance>(Outer);
	const bool bIsServerWorld = GameInstance->IsDedicatedServerInstance();
	return !bIsServerWorld;
}

void UTD_LoadingScreenManager::Tick(float DeltaTime)
{
}

ETickableTickType UTD_LoadingScreenManager::GetTickableTickType() const
{
	return ETickableTickType::Conditional;
}

bool UTD_LoadingScreenManager::IsTickable() const
{
	return !HasAnyFlags(RF_ClassDefaultObject);
}

TStatId UTD_LoadingScreenManager::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(UTD_LoadingScreenManager, STATGROUP_Tickables);
}

UWorld* UTD_LoadingScreenManager::GetTickableGameObjectWorld() const
{
	return GetGameInstance() != nullptr ? GetGameInstance()->GetWorld() : nullptr;
}

void UTD_LoadingScreenManager::AddLayerToScreen(const int32 InLayerID, TSubclassOf<UUserWidget> InWidget, const int32 ZOrder)
{
	if (WidgetLayersMap.Contains(InLayerID))
		return;
	
	if (InWidget.Get() && GetGameInstance())
	{
		if (UUserWidget* UserWidget = UUserWidget::CreateWidgetInstance(*GetGameInstance(), InWidget, NAME_None))
		{
			TSharedPtr<SWidget> LayerWidget = UserWidget->TakeWidget();

			UGameViewportClient* GameViewportClient = GetGameInstance()->GetGameViewportClient();
			GameViewportClient->AddViewportWidgetContent(LayerWidget.ToSharedRef(), ZOrder);

			WidgetLayersMap.Add(InLayerID, LayerWidget);
		}
	}
}

void UTD_LoadingScreenManager::RemoveLayerToScreen(const int32 InLayerID)
{
	TSharedPtr<SWidget>* LayerWidget = WidgetLayersMap.Find(InLayerID);
	if (LayerWidget && LayerWidget->Get())
	{
		UGameInstance* GI = GetGameInstance();
		if (GI)
		{
			UGameViewportClient* GameViewportClient = GI->GetGameViewportClient();
			GameViewportClient->RemoveViewportWidgetContent(LayerWidget->ToSharedRef());
			LayerWidget->Reset();
			WidgetLayersMap.Remove(InLayerID);
		}
	}
}

void UTD_LoadingScreenManager::HandlePreLoadMapNoContext(const FString& MapName)
{
}

void UTD_LoadingScreenManager::HandlePreLoadMap(const FWorldContext& WorldContext, const FString& MapName)
{
}

void UTD_LoadingScreenManager::HandlePostLoadMap(UWorld* World)
{
}

void UTD_LoadingScreenManager::HandleSeamlessTravelStart(UWorld* World, const FString& Map)
{
}
