#include "UdpSocketGameSubsystem.h"
#include "Common/UdpSocketBuilder.h"

#define print_string_red(PrintLog) GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, PrintLog);\
UE_LOG(LogTemp, Warning, TEXT("UdpSocket:: Red [%s]"), *PrintLog);

#define print_string_green(PrintLog) GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, PrintLog);\
UE_LOG(LogTemp, Warning, TEXT("UdpSocket:: Green [%s]"), *PrintLog);


const int32 UUdpSocketSubsystem::SendSize = 2 * 1024 * 1024;
const int32 UUdpSocketSubsystem::BufferSize = 2 * 1024 * 1024;

void UUdpSocketSubsystem::InitUdpSocket(const FString& ChosenSocketName)
{
	ISocketSubsystem* SocketSubsystem = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM);
	if (SocketSubsystem != nullptr)
	{
		if (Socket == nullptr)
		{
			LocalEndpoint = FIPv4Endpoint(FIPv4Address::Any, 12777);

			Socket = FUdpSocketBuilder(ChosenSocketName)
				.AsNonBlocking()
				.AsReusable()
				.BoundToEndpoint(LocalEndpoint)
				.WithSendBufferSize(SendSize)
				.WithReceiveBufferSize(BufferSize)
				.WithBroadcast();

			FString Log = FString(TEXT("UUdpSocketSubsystem::InitUdpSocket"));
			print_string_green(Log);
		}
	}
}

void UUdpSocketSubsystem::SetRemoteAddr(const FString& InRemoteIP, const int32 InRemotePort)
{
	RemotePort = InRemotePort;
	RemoteIP = InRemoteIP;
	FIPv4Address RemoteAddress;
	FIPv4Address::Parse(InRemoteIP, RemoteAddress);
	RemoteEndpoint = FIPv4Endpoint(RemoteAddress, InRemotePort);
}

bool UUdpSocketSubsystem::SendMessage(FString InMsg)
{
	if (!Socket) 
		return false;

	int32 BytesSent;
	TCHAR* serializedChar = InMsg.GetCharArray().GetData();
	int32 size = FCString::Strlen(serializedChar);
	bool success = Socket->SendTo((uint8*)TCHAR_TO_UTF8(serializedChar), size, BytesSent, *RemoteEndpoint.ToInternetAddr());

	UE_LOG(LogTemp, Warning, TEXT("UUdpSocketSubsystem:: Sent message: %s : %s : Address - %s : BytesSent - %d"), *InMsg, (success ? TEXT("true") : TEXT("false")), *RemoteEndpoint.ToString(), BytesSent);
	return success && BytesSent > 0;
}

FString UUdpSocketSubsystem::GetRemoteAddr() const
{
	return RemoteEndpoint.ToString();
}

UWorld* UUdpSocketSubsystem::GetCurrentWorld() const
{
	UGameInstance* GI = GetGameInstance();
	if (GI)
	{
		return GI->GetWorld();
	}
	return nullptr;
}

bool UUdpSocketSubsystem::ShouldCreateSubsystem(UObject* Outer) const
{
	const UGameInstance* GameInstance = CastChecked<UGameInstance>(Outer);
	const bool bIsServerWorld = GameInstance->IsDedicatedServerInstance();
	return !bIsServerWorld;
}

void UUdpSocketSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Socket = nullptr;
	InitUdpSocket(TEXT("UdpSocketSubsystem"));
}

void UUdpSocketSubsystem::Deinitialize()
{
	ISocketSubsystem* SocketSubsystem = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM);
	if (SocketSubsystem != nullptr)
	{
		SocketSubsystem->DestroySocket(Socket);
		Socket = nullptr;
		FString Log = FString("UUdpSocketSubsystem::Deinitialize");
		print_string_green(Log);
	}
}

UWorld* UUdpSocketSubsystem::GetTickableGameObjectWorld() const
{
	return GetCurrentWorld();
}

ETickableTickType UUdpSocketSubsystem::GetTickableTickType() const
{
	return IsTemplate() ? ETickableTickType::Never : FTickableGameObject::GetTickableTickType();
}

bool UUdpSocketSubsystem::IsAllowedToTick() const
{
	return !IsTemplate() && Socket != nullptr;
}

void UUdpSocketSubsystem::Tick(float DeltaTime)
{
	if (Socket == nullptr)
		return;

	ListenMessage();
}

TStatId UUdpSocketSubsystem::GetStatId() const
{
	return TStatId();
}

void UUdpSocketSubsystem::ListenMessage()
{
	if (Socket == nullptr)
		return;
	
	TSharedRef<FInternetAddr> targetAddr = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	uint32 Size;
	if (Socket->HasPendingData(Size))
	{
		uint8* Recv = new uint8[Size];
		int32 BytesRead = 0;

		TArray<uint8> ReceivedData;
		ReceivedData.SetNumUninitialized(FMath::Min(Size, 65507u));
		Socket->RecvFrom(ReceivedData.GetData(), ReceivedData.Num(), BytesRead, *targetAddr);

		char ansiiData[1024];
		memcpy(ansiiData, ReceivedData.GetData(), BytesRead);
		ansiiData[BytesRead] = 0;

		FString IpPort = targetAddr->ToString(true);
		print_string_red(IpPort);

		FString StrL, StrR;
		if (IpPort.Split(TEXT(":"), &StrL, &StrR))
		{
			SetRemoteAddr(StrL, FCString::Atoi(*StrR));
		}

		FString data = ANSI_TO_TCHAR(ansiiData);
		print_string_green(data);

		UE_LOG(LogTemp, Warning, TEXT("UUdpSocketSubsystem:: message: %s  Address - %s"), *data, *IpPort);
		OnReceiverMsg.Broadcast(data);
	}
}