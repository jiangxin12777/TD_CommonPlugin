#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "Common/UdpSocketReceiver.h"
#include "GameFramework/Actor.h"
#include "Tickable.h"
#include "UdpSocketGameSubsystem.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnReceiverMsg, FString, Msg);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnReceiverMsg, FString, Msg);

UCLASS()
class TD_UDPSOCKET_API UUdpSocketSubsystem : public UGameInstanceSubsystem, public FTickableGameObject
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, Category = "UdpSocketSubsystem")
	void InitUdpSocket(const FString& ChosenSocketName);

	UFUNCTION(BlueprintCallable, Category = "UdpSocketSubsystem")
	void SetRemoteAddr(const FString& InRemoteIP, const int32 InRemotePort);

	UFUNCTION(BlueprintCallable, Category = "UdpSocketSubsystem")
	bool SendMessage(FString InMsg);

	UFUNCTION(BlueprintCallable, Category = "UdpSocketSubsystem")
	FString GetRemoteAddr() const;

	UWorld* GetCurrentWorld() const;

public:
	UPROPERTY(BlueprintAssignable, Category = "UdpSocketSubsystem")
	FOnReceiverMsg OnReceiverMsg;

protected:
	// Begin USubsystem Interface
	virtual bool ShouldCreateSubsystem(UObject* Outer) const override;
	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	// End USubsystem Interface

	// FTickableGameObject implementation Begin
	virtual UWorld* GetTickableGameObjectWorld() const override;
	virtual ETickableTickType GetTickableTickType() const override;
	virtual bool IsAllowedToTick() const override final;
	virtual void Tick(float DeltaTime) override;
	virtual TStatId GetStatId() const override;
	// FTickableGameObject implementation End

	void ListenMessage();

protected:
	/** 远端端口 */
	UPROPERTY(BlueprintReadOnly, Category = "UdpSocketSubsystem")
	int32 RemotePort;

	/** 远端IP */
	UPROPERTY(BlueprintReadOnly, Category = "UdpSocketSubsystem")
	FString RemoteIP = "127.0.0.1";

	/** 本地端口 */
	UPROPERTY(BlueprintReadOnly, Category = "UdpSocketSubsystem")
	int32 LocalPort;


private:
	/** 套接字对象 */
	FSocket* Socket;

	/** 远端地址 */
	FIPv4Endpoint RemoteEndpoint;

	/** 本地地址 */
	FIPv4Endpoint LocalEndpoint;

	static const int32 SendSize;
	static const int32 BufferSize;
};